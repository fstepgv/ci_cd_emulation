# Gitlab CI emulation  project

## Description

The project is  based on a simple JS app that shows webpage on a desired port.

The project demonstrates  full DevOps pipeline - from running initial tests on the app itself   to deploying a dockerized container with App  to production on AWS.

Gitlab CI is using 2x runners on AWS EC2 instance-1.  Docker and shell executors are used , depending on requirements. 

Pipeline starts only during commits/merge requests to main branch of project. 

More detailed pipeline description:

#### Phase 1. Tests.

The pipeline is running  SAST and LINT tests for app  code  (tests provided by Gitlab)
Also one more test  uses Jest test library to check index.html file presence in app folder.


#### Phase 2. Build.

Pipeline builds simple JS app as Docker container.

#### Phase 3. Push.

Docker container is being pushed to Gitlab registry.

#### Phase 4. Deploy to Dev and run tests.

The build is being deployed to emulated dev environment on AWS EC2 instance-2 . App is running on port 3000 and dev test emulation is being performed.

#### Phase 5. Deploy to Staging and run tests.

If dev tests are passed fine , build is being deployed to emulated staging environment on AWS EC2 instance-2 . App is running on port 3000 and app performance test emulation is being performed.

#### Phase 6. Manual Deploy to Production.

The App is ready to be deployed to prod and is waiting for manual approval. After manual approval, app is being  deployed to AWS EC2 instance-2 and starts on port 5000.

```mermaid
graph TD
    A[Tests] -->|SAST/LINT| B(Build)
    B --> C(Push to Registry)
    C --> D{Deploy to}
    D --> E(DEV)
    D --> F(STAGE)
    D --> G(PROD)
    E --> H(run tests)
    F --> I(run tests)
    G --> K(manual approve)

```




Example of successful pipeline for review: [link](https://gitlab.com/fstepgv/ci_cd_emulation/-/pipelines/626881665)

Gitlab-CI.yml for review: [link](https://gitlab.com/fstepgv/ci_cd_emulation/-/blob/main/.gitlab-ci.yml)

